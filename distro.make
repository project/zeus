; Use this file to build a full distribution including Drupal core and the
; Zeus install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7"

; Add Zeus to the full distribution build.
projects[zeus][type] = profile
projects[zeus][version] = 1.x-dev
projects[zeus][download][type] = git
projects[zeus][download][url] = http://git.drupal.org/project/zeus.git
projects[zeus][download][branch] = 7.x-1.x
