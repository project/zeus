; Drupal.org release file.
core = 7.38
api = 2

; Modules
projects[admin_menu][version] = "3.0-rc5"
projects[adminimal_admin_menu][version] = "1.5"
projects[ctools][version] = "1.7"
projects[computed_field][version] = "1.x-dev"
projects[devel][version] = "1.5"
projects[entity][version] = "1.6"
projects[entityreference][version] = "1.1"
projects[jquery_update][version] = "3.0-alpha2"
projects[libraries][version] = "2.2"
projects[menu_breadcrumb][version] = "1.6"
projects[menu_target][version] = "1.4"
projects[mobile_detect][version] = "1.x-dev"
projects[mobile_switch][version] = "2.0-beta1"
projects[module_filter][version] = "2.0"
projects[navbar][version] = "1.6"
projects[path_breadcrumbs][version] = "3.2"
projects[pathauto][version] = "1.2"
projects[token][version] = "1.6"
projects[views][version] = "3.11"
projects[views_field_view][version] = "1.1"

; Themes
projects[adminimal_theme][version] = "1.21"
projects[bootstrap][version] = "3.x-dev"

; Libraries
; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[superfish][download][type] = "get"
libraries[superfish][download][url] = "https://github.com/mehrpadin/Superfish-for-Drupal/archive/master.zip"
libraries[superfish][directory_name] = "superfish"
libraries[superfish][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[modernizr][download][type] = "get"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/master.zip"
libraries[modernizr][directory_name] = "modernizr"
libraries[modernizr][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[backbone][download][type] = "get"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/master.zip"
libraries[backbone][directory_name] = "backbone"
libraries[backbone][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[Mobile_Detect][download][type] = "get"
libraries[Mobile_Detect][download][url] = "https://github.com/serbanghita/Mobile-Detect/archive/master.zip"
libraries[Mobile_Detect][directory_name] = "Mobile_Detect"
libraries[Mobile_Detect][type] = "library"

; Please fill the following out. Type may be one of get, git, bzr or svn,
; and url is the url of the download.
libraries[underscore][download][type] = "get"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/master.zip"
libraries[underscore][directory_name] = "underscore"
libraries[underscore][type] = "library"
