<?php

function zeus_admin() {
    $form = array();
    return $form;
}

function zeus_admin_plugins() {
    $form = array();

    zeus_get_module_apis('zeus');
    zeus_module_include('zeus');
    $plugin_groups = zeus_discover_plugins();

    foreach ($plugin_groups as $type => $plugins) {
        $form['plugins_' . $type] = array(
            '#type' => 'fieldset',
            '#title' => ucfirst($type) . ' plugins',
            '#collapsible' => true,
            '#collapsed' => false,
        );

        $options = array();
        foreach ($plugins as $plugin) {
            $options[$plugin['name']] = '<b>' . $plugin['title'] . '</b>: ' . $plugin['description'];
        }

        $form['plugins_' . $type]['zeus_plugin_status_' . $type] = array(
            '#type' => 'checkboxes',
            '#title' => 'Enable',
            '#options' => $options,
            '#default_value' => variable_get('zeus_plugin_status_' . $type, array_keys($options)),
        );
    }

    return system_settings_form($form);
}
