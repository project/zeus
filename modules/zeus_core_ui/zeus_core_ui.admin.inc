<?php

function zeus_core_ui_admin() {
    $form = array();
    return $form;
}

function zeus_core_ui_admin_plugins() {
    $form = array();

    zeus_core_get_module_apis('zeus_core');
    zeus_core_module_include('zeus_core');
    $plugin_groups = zeus_core_discover_plugins();

    foreach ($plugin_groups as $type => $plugins) {
        $form['plugins_' . $type] = array(
            '#type' => 'fieldset',
            '#title' => ucfirst($type) . ' plugins',
            '#collapsible' => true,
            '#collapsed' => false,
        );

        $options = array();
        foreach ($plugins as $plugin) {
            $options[$plugin['name']] = '<b>' . $plugin['title'] . '</b>: ' . $plugin['description'];
        }

        $form['plugins_' . $type]['zeus_plugin_status_' . $type] = array(
            '#type' => 'checkboxes',
            '#options' => $options,
            '#default_value' => variable_get('zeus_plugin_status_' . $type, array_keys($options)),
        );
    }

    return system_settings_form($form);
}

function zeus_core_ui_create_site_from_template($form, &$form_state, $node) { 
    $root_path = $node->field_template_doc_root[$node->language][0]['value'] . '/';
    $form_state['storage']['template_node'] = $node;
    $form['name'] = array(
        '#type' => 'textfield',
        '#title' => 'Name',
        '#required' => true,
        '#default_value' => 'zeus-' . zeus_core_random_string(10),
        '#description' => $root_path . '&lt;<em>URL</em>&gt; must not exist',
    );

    $distro = node_load($node->field_distribution_ref[$node->language][0]['target_id']);
    $options = array();
    foreach ($distro->field_distribution_profiles[$distro->language] as $profile) {
        $options[] = $profile['value'];
    }

    $form['profile'] = array(
        '#type' => 'radios',
        '#title' => 'Profile',
        '#required' => true,
        '#default_value' => $options[0],
        '#options' => drupal_map_assoc($options),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Create site',
    );
    return $form;
}

function zeus_core_ui_create_site_from_template_validate($form, &$form_state) {
    if (!preg_match('/^[a-z0-9][a-z0-9-]+$/', $form_state['values']['name'])) {
        form_set_error('name', 'Valid characters: lower-case alphabets, numbers, dashes. Must start with an alphabet or number.');
        return;
    }

    if (strlen($form_state['values']['name']) > 30) {
        form_set_error('name', 'Please choose a name up to 30 characters long.');
        return;
    }

    $template = $form_state['storage']['template_node'];
    $root_path = $template->field_template_doc_root[$template->language][0]['value'] . '/';
    if (file_exists($root_path . $form_state['values']['name'])) {
        form_set_error('name', $root_path . ' already exists.');
    }
}

function zeus_core_ui_create_site_from_template_submit($form, &$form_state) {
    $node = zeus_core_create_site_node(array(
        'site-name' => $form_state['values']['name'],
        'template' => $form_state['storage']['template_node'],
        'profile' => $form_state['values']['profile'],
    ));

    if ($node) {
        drupal_goto('node/'. $node->nid);
    } else {
        drupal_set_message('An error occured while creating the site.', 'error');
    }
}
