<?php

class zeus_create_user extends zeus_core_template_plugin {
    public function configure_site($site) {
        parent::configure_site($site);

        $site_wrapper = entity_metadata_wrapper('node', $site);
        $template_wrapper = entity_metadata_wrapper('node', $site_wrapper->field_template_ref->value());
        $args = array();
        $args[] = '--root=' . $template_wrapper->field_template_doc_root->value() . '/' . $site_wrapper->field_site_name->value();

        $args = join(' ', $args);
        $users = array();
        foreach($template_wrapper->field_template_users->value() as $item) {
            $item_wrapper = entity_metadata_wrapper('field_collection_item', $item);
            $user_wrapper = entity_metadata_wrapper('user', $item_wrapper->field_template_user_ref->value());

            $cmd[] = escapeshellcmd('drush ' . $args . ' user-create ' . escapeshellarg($user_wrapper->name->value()) .
                     ' --mail=' . escapeshellarg($user_wrapper->mail->value()) .
                     ' --password=' . escapeshellarg(zeus_core_random_string()) .  // TODO: Allow this to be configurable
                     ' -y');

            if ($item_wrapper->field_template_user_status->value() == 'blocked') {
                $cmd[] = escapeshellcmd('drush ' . $args . ' user-block ' . escapeshellarg($user_wrapper->name->value()));
            }
        }

        drupal_alter('zeus_drush_command', $cmd);

        $cmd = join(' && ', $cmd);
        zeus_core_execute_drush_command($cmd);
    }
}
