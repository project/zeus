<?php

function zeus_create_user_install() {
    node_types_rebuild();

    // Create fields
    foreach (_zeus_create_user_installed_fields() as $field) {
        field_create_field($field);
    }
 
    // Associate our fields with our content types
    foreach (_zeus_create_user_installed_instances() as $instance) {
        field_create_instance($instance);
    }
}

function _zeus_create_user_installed_fields() {
    return array(
        // Create field_template_users definition
        'field_template_users' => array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(
                'hide_blank_items' => 1,
                'path' => '',
            ),
            'foreign keys' => array(),
            'indexes' => array(
                'revision_id' => array(
                    0 => 'revision_id',
                ),
            ),
            'field_name' => 'field_template_users',
            'type' => 'field_collection',
            'module' => 'field_collection',
            'active' => '1',
            'locked' => '0',
            'cardinality' => '-1',
            'columns' => array(
                'value' => array(
                    'type' => 'int',
                    'not null' => FALSE,
                    'description' => 'The field collection item id.',
                ),
                'revision_id' => array(
                    'type' => 'int',
                    'not null' => FALSE,
                    'description' => 'The field collection item revision id.',
                ),
            ),
            'bundles' => array(
                'node' => array(
                    0 => 'site_template',
                ),
            ),
        ),

        // Create field_template_user_ref definition
        'field_template_user_ref' => array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(
                'target_type' => 'user',
                'handler' => 'base',
                'handler_settings' => array(
                    'target_bundles' => array(),
                    'sort' => array(
                        'type' => 'property',
                        'property' => 'name',
                        'direction' => 'DESC',
                    ),
                    'behaviors' => array(
                        'views-select-list' => array(
                            'status' => 0,
                        ),
                    ),
                ),
            ),
            'foreign keys' => array(
                'users' => array(
                    'table' => 'users',
                    'columns' => array(
                        'target_id' => 'uid',
                    ),
                ),
            ),
            'indexes' => array(
                'target_id' => array(
                    0 => 'target_id',
                ),
            ),
            'field_name' => 'field_template_user_ref',
            'type' => 'entityreference',
            'module' => 'entityreference',
            'active' => '1',
            'locked' => '0',
            'cardinality' => '1',
            'deleted' => '0',
            'columns' => array(
                'target_id' => array(
                    'description' => 'The id of the target entity.',
                    'type' => 'int',
                    'unsigned' => TRUE,
                    'not null' => TRUE,
                ),
            ),
            'bundles' => array(
                'field_collection_item' => array(
                    0 => 'field_template_users',
                ),
            ),
        ),

        // Create field_template_user_status definition
        'field_template_user_status' => array(
            'translatable' => '0',
            'entity_types' => array(),
            'settings' => array(
                'allowed_values' => array(
                    'active' => 'Active',
                    'blocked' => 'Blocked',
                ),
                'allowed_values_function' => '',
            ),
            'foreign keys' => array(),
            'indexes' => array(
                'value' => array(
                    0 => 'value',
                ),
            ),
            'field_name' => 'field_template_user_status',
            'type' => 'list_text',
            'module' => 'list',
            'active' => '1',
            'locked' => '0',
            'cardinality' => '1',
            'deleted' => '0',
            'columns' => array(
                'value' => array(
                    'type' => 'varchar',
                    'length' => 255,
                    'not null' => FALSE,
                ),
            ),
            'bundles' => array(
                'field_collection_item' => array(
                    0 => 'field_template_users',
                ),
            ),
        ),
    );
}

function _zeus_create_user_installed_instances() {
    $t = get_t();
    return array( 
        // Create field_template_users instance
        'field_template_users' => array(
            'label' => 'Users',
            'widget' => array(
                'weight' => '11',
                'type' => 'field_collection_embed',
                'module' => 'field_collection',
                'active' => 0,
                'settings' => array(),
            ),
            'settings' => array(
                'user_register_form' => FALSE,
            ),
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'type' => 'field_collection_view',
                    'settings' => array(
                        'edit' => 'Edit',
                        'delete' => 'Delete',
                        'add' => 'Add',
                        'description' => TRUE,
                        'view_mode' => 'full',
                    ),
                    'module' => 'field_collection',
                    'weight' => 7,
                ),
                'teaser' => array(
                    'type' => 'hidden',
                    'label' => 'above',
                    'settings' => array(),
                    'weight' => 0,
                ),
            ),
            'required' => 0,
            'description' => '',
            'default_value' => NULL,
            'field_name' => 'field_template_users',
            'entity_type' => 'node',
            'bundle' => 'site_template',
        ),

        // Create field_template_user_ref instance
        'field_template_user_ref' => array(
            'label' => 'User',
            'widget' => array(
                'weight' => '13',
                'type' => 'entityreference_autocomplete',
                'module' => 'entityreference',
                'active' => 1,
                'settings' => array(
                    'match_operator' => 'CONTAINS',
                    'size' => '60',
                    'path' => '',
                ),
            ),
            'settings' => array(
                'user_register_form' => FALSE,
            ),
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'type' => 'entityreference_label',
                    'settings' => array(
                        'link' => FALSE,
                    ),
                    'module' => 'entityreference',
                    'weight' => 8,
                ),
                'teaser' => array(
                    'type' => 'hidden',
                    'label' => 'above',
                    'settings' => array(),
                    'weight' => 0,
                ),
            ),
            'required' => 1,
            'description' => '',
            'default_value' => NULL,
            'field_name' => 'field_template_user_ref',
            'entity_type' => 'field_collection_item',
            'bundle' => 'field_template_users',
        ),

        // Create field_template_user_status instance
        'field_template_user_status' => array(
            'label' => 'Status',
            'widget' => array(
                'weight' => '15',
                'type' => 'options_buttons',
                'module' => 'options',
                'active' => 1,
                'settings' => array(),
            ),
            'settings' => array(
                'user_register_form' => FALSE,
            ),
            'display' => array(
                'default' => array(
                    'label' => 'above',
                    'type' => 'list_default',
                    'settings' => array(),
                    'module' => 'list',
                    'weight' => 9,
                ),
                'teaser' => array(
                    'type' => 'hidden',
                    'label' => 'above',
                    'settings' => array(),
                    'weight' => 0,
                ),
            ),
            'required' => 1,
            'description' => '',
            'default_value' => NULL,
            'field_name' => 'field_template_user_status',
            'entity_type' => 'field_collection_item',
            'bundle' => 'field_template_users',
        ),
    );
}

function zeus_create_user_uninstall() {
    // Delete field instances
    $fields = _zeus_create_user_installed_instances();
    foreach ($fields as $field) {
        field_delete_instance($field);
    }

    // Delete field definitions
    $fields = array_keys(_zeus_create_user_installed_fields());
    foreach ($fields as $field) {
        field_delete_field($field);
    }

    // Purge all field infromation
    field_purge_batch(1000);
}
