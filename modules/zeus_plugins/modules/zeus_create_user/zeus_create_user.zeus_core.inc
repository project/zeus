<?php

/**
 * Implements hook_zeus_plugins().
 */
function zeus_create_user_zeus_plugins() {
    return array(
        'template' => array(
            'zeus_create_user' => array(
                'title' => 'Create users',
                'description' => 'Create users.',
                'version' => 1,
                'path' => drupal_get_path('module', 'zeus_create_user') . '/plugins',
                'theme' => 'zeus_plugin_theme_template',
                'theme path' => drupal_get_path('module', 'zeus_create_user') . '/plugins/template',
            ),
        ),
    );
}
