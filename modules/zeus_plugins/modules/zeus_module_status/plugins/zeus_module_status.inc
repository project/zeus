<?php

class zeus_module_status extends zeus_core_template_plugin {
    public function configure_site($site) {
        parent::configure_site($site);

        $site_wrapper = entity_metadata_wrapper('node', $site);
        $template_wrapper = entity_metadata_wrapper('node', $site_wrapper->field_template_ref->value());
        $args = array();
        $args[] = '--root=' . $template_wrapper->field_template_doc_root->value() . '/' . $site_wrapper->field_site_name->value();

        $modules = array('enabled' => array(), 'disabled' => array());
        foreach($template_wrapper->field_template_module_status->value() as $item) {
            $item_wrapper = entity_metadata_wrapper('field_collection_item', $item);
            $modules[$item_wrapper->field_module_enabled->value()][] = $item_wrapper->field_module_name->value();
        }

        $cmd = array();
        if (!empty($modules['enabled'])) {
            $cmd[] = escapeshellcmd('drush ' . join(' ', $args) . ' en ' . join(' ', $modules['enabled']) . ' -y');
        }
        if (!empty($modules['disabled'])) {
            $cmd[] = escapeshellcmd('drush ' . join(' ', $args) . ' dis ' . join(' ', $modules['disabled']) . ' -y');
        }

        drupal_alter('zeus_drush_command', $cmd);

        $cmd = join(' && ', $cmd);
        zeus_core_execute_drush_command($cmd);
    }
}
