<?php

/**
 * Implements hook_zeus_plugins().
 */
function zeus_module_status_zeus_plugins() {
    return array(
        'template' => array(
            'zeus_module_status' => array(
                'title' => 'Enable/disable modules',
                'description' => 'Enable or disable certain modules.',
                'version' => 1,
                'path' => drupal_get_path('module', 'zeus_module_status') . '/plugins',
                'theme' => 'zeus_plugin_theme_template',
                'theme path' => drupal_get_path('module', 'zeus_module_status') . '/plugins/template',
            ),
        ),
    );
}
