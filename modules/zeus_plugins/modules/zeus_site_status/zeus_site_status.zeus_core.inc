<?php

/**
 * Implements hook_zeus_plugins().
 */
function zeus_site_status_zeus_plugins() {
    return array(
        'status' => array(
            'zeus_site_status_updown' => array(
                'title' => 'Up/Down status',
                'description' => 'Indicates whether a site is up or down.',
                'version' => 1,
                'path' => drupal_get_path('module', 'zeus_site_status') . '/plugins',
                'theme' => 'zeus_plugin_theme_template',
                'theme path' => drupal_get_path('module', 'zeus_site_status') . '/plugins/template',
            ),
        ),
    );
}
