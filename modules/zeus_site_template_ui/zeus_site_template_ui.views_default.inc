<?php

/**
 * Implements hook_views_default_views().
 */
function zeus_site_template_ui_views_default_views() {
    $view = new view();
    $view->name = 'templates';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Templates';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE;

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Templates';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'nid' => 'nid',
      'title' => 'title',
      'view' => 'title',
      'nothing_1' => 'title',
      'expression' => 'title',
      'nothing' => 'nothing',
      'edit_node' => 'nothing',
      'delete_node' => 'nothing',
      'created' => 'nothing',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
      'nid' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'title' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'view' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'nothing_1' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'expression' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'nothing' => array(
        'align' => '',
        'separator' => ' | ',
        'empty_column' => 0,
      ),
      'edit_node' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'delete_node' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'created' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    /* No results behavior: Global: Text area */
    $handler->display->display_options['empty']['area']['id'] = 'area';
    $handler->display->display_options['empty']['area']['table'] = 'views';
    $handler->display->display_options['empty']['area']['field'] = 'area';
    $handler->display->display_options['empty']['area']['empty'] = TRUE;
    $handler->display->display_options['empty']['area']['content'] = 'You have not created any templates yet.';
    $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
    /* Field: Content: Nid */
    $handler->display->display_options['fields']['nid']['id'] = 'nid';
    $handler->display->display_options['fields']['nid']['table'] = 'node';
    $handler->display->display_options['fields']['nid']['field'] = 'nid';
    $handler->display->display_options['fields']['nid']['label'] = '';
    $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
    $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = 'Template';
    $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = TRUE;
    $handler->display->display_options['fields']['title']['alter']['strip_tags'] = TRUE;
    /* Field: Global: View */
    $handler->display->display_options['fields']['view']['id'] = 'view';
    $handler->display->display_options['fields']['view']['table'] = 'views';
    $handler->display->display_options['fields']['view']['field'] = 'view';
    $handler->display->display_options['fields']['view']['label'] = '';
    $handler->display->display_options['fields']['view']['exclude'] = TRUE;
    $handler->display->display_options['fields']['view']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['view']['view'] = 'sites';
    $handler->display->display_options['fields']['view']['display'] = 'page_2';
    $handler->display->display_options['fields']['view']['arguments'] = '[!nid]';
    /* Field: Total sites */
    $handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
    $handler->display->display_options['fields']['nothing_1']['table'] = 'views';
    $handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
    $handler->display->display_options['fields']['nothing_1']['ui_name'] = 'Total sites';
    $handler->display->display_options['fields']['nothing_1']['exclude'] = TRUE;
    $handler->display->display_options['fields']['nothing_1']['alter']['text'] = '[view]';
    $handler->display->display_options['fields']['nothing_1']['alter']['trim_whitespace'] = TRUE;
    $handler->display->display_options['fields']['nothing_1']['alter']['strip_tags'] = TRUE;
    /* Field: Global: Math expression */
    $handler->display->display_options['fields']['expression']['id'] = 'expression';
    $handler->display->display_options['fields']['expression']['table'] = 'views';
    $handler->display->display_options['fields']['expression']['field'] = 'expression';
    $handler->display->display_options['fields']['expression']['alter']['make_link'] = TRUE;
    $handler->display->display_options['fields']['expression']['alter']['path'] = 'node/[nid]/sites';
    $handler->display->display_options['fields']['expression']['precision'] = '0';
    $handler->display->display_options['fields']['expression']['separator'] = '';
    $handler->display->display_options['fields']['expression']['format_plural'] = TRUE;
    $handler->display->display_options['fields']['expression']['format_plural_singular'] = '1 site';
    $handler->display->display_options['fields']['expression']['format_plural_plural'] = '@count sites';
    $handler->display->display_options['fields']['expression']['prefix'] = ' (';
    $handler->display->display_options['fields']['expression']['suffix'] = ')';
    $handler->display->display_options['fields']['expression']['expression'] = '[nothing_1]';
    /* Field: Create link */
    $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['table'] = 'views';
    $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
    $handler->display->display_options['fields']['nothing']['ui_name'] = 'Create link';
    $handler->display->display_options['fields']['nothing']['label'] = '';
    $handler->display->display_options['fields']['nothing']['alter']['text'] = 'create site';
    $handler->display->display_options['fields']['nothing']['alter']['make_link'] = TRUE;
    $handler->display->display_options['fields']['nothing']['alter']['path'] = 'node/[nid]/create';
    $handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
    /* Field: Content: Edit link */
    $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
    /* Field: Content: Delete link */
    $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
    /* Field: Content: Post date */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = 'node';
    $handler->display->display_options['fields']['created']['field'] = 'created';
    $handler->display->display_options['fields']['created']['label'] = 'Created';
    $handler->display->display_options['fields']['created']['date_format'] = 'long';
    $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
    /* Sort criterion: Content: Title */
    $handler->display->display_options['sorts']['title']['id'] = 'title';
    $handler->display->display_options['sorts']['title']['table'] = 'node';
    $handler->display->display_options['sorts']['title']['field'] = 'title';
    /* Filter criterion: Content: Type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'node';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['value'] = array(
      'site_template' => 'site_template',
    );
    $handler->display->display_options['filters']['type']['group'] = 1;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['value'] = 1;
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'templates';
    $handler->display->display_options['menu']['type'] = 'normal';
    $handler->display->display_options['menu']['title'] = 'Templates';
    $handler->display->display_options['menu']['weight'] = '1';
    $handler->display->display_options['menu']['name'] = 'main-menu';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
    $views[$view->name] = $view;

    cache_clear_all('*', 'cache_menu', TRUE);
    return $views;
}
