<?php

/**
 * Implements hook_views_default_views().
 */
function zeus_site_ui_views_default_views() {
    $view = new view();
    $view->name = 'sites';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'node';
    $view->human_name = 'Sites';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE;

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['title'] = 'Sites';
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'perm';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['pager']['options']['items_per_page'] = '10';
    $handler->display->display_options['style_plugin'] = 'table';
    $handler->display->display_options['style_options']['columns'] = array(
      'nid' => 'nid',
      'title' => 'title',
      'field_site_url' => 'field_site_url',
      'field_template_ref' => 'field_template_ref',
      'delete_node' => 'delete_node',
      'edit_node' => 'edit_node',
      'created' => 'created',
    );
    $handler->display->display_options['style_options']['default'] = '-1';
    $handler->display->display_options['style_options']['info'] = array(
      'nid' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'title' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'field_site_url' => array(
        'sortable' => 0,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'field_template_ref' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'delete_node' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'edit_node' => array(
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
      'created' => array(
        'sortable' => 1,
        'default_sort_order' => 'asc',
        'align' => '',
        'separator' => '',
        'empty_column' => 0,
      ),
    );
    /* No results behavior: Global: Text area */
    $handler->display->display_options['empty']['area']['id'] = 'area';
    $handler->display->display_options['empty']['area']['table'] = 'views';
    $handler->display->display_options['empty']['area']['field'] = 'area';
    $handler->display->display_options['empty']['area']['empty'] = TRUE;
    $handler->display->display_options['empty']['area']['content'] = 'You have not created any sites yet.';
    $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
    /* Field: Content: Nid */
    $handler->display->display_options['fields']['nid']['id'] = 'nid';
    $handler->display->display_options['fields']['nid']['table'] = 'node';
    $handler->display->display_options['fields']['nid']['field'] = 'nid';
    $handler->display->display_options['fields']['nid']['label'] = '';
    $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
    $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['label'] = 'Site';
    /* Field: Content: Site URL */
    $handler->display->display_options['fields']['field_site_url']['id'] = 'field_site_url';
    $handler->display->display_options['fields']['field_site_url']['table'] = 'field_data_field_site_url';
    $handler->display->display_options['fields']['field_site_url']['field'] = 'field_site_url';
    $handler->display->display_options['fields']['field_site_url']['field_api_classes'] = TRUE;
    /* Field: Content: Template */
    $handler->display->display_options['fields']['field_template_ref']['id'] = 'field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['table'] = 'field_data_field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['field'] = 'field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['settings'] = array(
      'link' => 1,
    );
    /* Field: Content: Edit link */
    $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
    /* Field: Content: Delete link */
    $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['label'] = 'Delete';
    /* Field: Content: Post date */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = 'node';
    $handler->display->display_options['fields']['created']['field'] = 'created';
    $handler->display->display_options['fields']['created']['label'] = 'Created';
    $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
    $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
    /* Field: Content: Site Status */
    $handler->display->display_options['fields']['field_site_status']['id'] = 'field_site_status';
    $handler->display->display_options['fields']['field_site_status']['table'] = 'field_data_field_site_status';
    $handler->display->display_options['fields']['field_site_status']['field'] = 'field_site_status';
    $handler->display->display_options['fields']['field_site_status']['label'] = 'Status';
    /* Sort criterion: Content: Post date */
    $handler->display->display_options['sorts']['created']['id'] = 'created';
    $handler->display->display_options['sorts']['created']['table'] = 'node';
    $handler->display->display_options['sorts']['created']['field'] = 'created';
    $handler->display->display_options['sorts']['created']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['sorts']['created']['order'] = 'DESC';
    /* Filter criterion: Content: Type */
    $handler->display->display_options['filters']['type']['id'] = 'type';
    $handler->display->display_options['filters']['type']['table'] = 'node';
    $handler->display->display_options['filters']['type']['field'] = 'type';
    $handler->display->display_options['filters']['type']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['filters']['type']['value'] = array(
      'site' => 'site',
    );
    $handler->display->display_options['filters']['type']['group'] = 1;
    /* Filter criterion: Content: Published */
    $handler->display->display_options['filters']['status']['id'] = 'status';
    $handler->display->display_options['filters']['status']['table'] = 'node';
    $handler->display->display_options['filters']['status']['field'] = 'status';
    $handler->display->display_options['filters']['status']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['filters']['status']['value'] = '1';
    $handler->display->display_options['filters']['status']['group'] = 1;
    $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;

    /* Display: Page */
    $handler = $view->new_display('page', 'Page', 'page');
    $handler->display->display_options['path'] = 'websites';
    $handler->display->display_options['menu']['type'] = 'normal';
    $handler->display->display_options['menu']['title'] = 'Sites';
    $handler->display->display_options['menu']['weight'] = '2';
    $handler->display->display_options['menu']['name'] = 'main-menu';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;

    /* Display: Template tab */
    $handler = $view->new_display('page', 'Template tab', 'page_1');
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Content: Nid */
    $handler->display->display_options['fields']['nid']['id'] = 'nid';
    $handler->display->display_options['fields']['nid']['table'] = 'node';
    $handler->display->display_options['fields']['nid']['field'] = 'nid';
    $handler->display->display_options['fields']['nid']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['nid']['label'] = '';
    $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
    $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
    /* Field: Content: Title */
    $handler->display->display_options['fields']['title']['id'] = 'title';
    $handler->display->display_options['fields']['title']['table'] = 'node';
    $handler->display->display_options['fields']['title']['field'] = 'title';
    $handler->display->display_options['fields']['title']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['title']['label'] = 'Site';
    /* Field: Content: Site URL */
    $handler->display->display_options['fields']['field_site_url']['id'] = 'field_site_url';
    $handler->display->display_options['fields']['field_site_url']['table'] = 'field_data_field_site_url';
    $handler->display->display_options['fields']['field_site_url']['field'] = 'field_site_url';
    $handler->display->display_options['fields']['field_site_url']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['field_site_url']['field_api_classes'] = TRUE;
    /* Field: Content: Template */
    $handler->display->display_options['fields']['field_template_ref']['id'] = 'field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['table'] = 'field_data_field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['field'] = 'field_template_ref';
    $handler->display->display_options['fields']['field_template_ref']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['field_template_ref']['settings'] = array(
      'link' => 1,
    );
    /* Field: Content: Edit link */
    $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
    $handler->display->display_options['fields']['edit_node']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['edit_node']['label'] = 'Edit';
    /* Field: Content: Delete link */
    $handler->display->display_options['fields']['delete_node']['id'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['table'] = 'views_entity_node';
    $handler->display->display_options['fields']['delete_node']['field'] = 'delete_node';
    $handler->display->display_options['fields']['delete_node']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['delete_node']['label'] = 'Delete';
    /* Field: Content: Post date */
    $handler->display->display_options['fields']['created']['id'] = 'created';
    $handler->display->display_options['fields']['created']['table'] = 'node';
    $handler->display->display_options['fields']['created']['field'] = 'created';
    $handler->display->display_options['fields']['created']['relationship'] = 'reverse_field_template_ref_node';
    $handler->display->display_options['fields']['created']['label'] = 'Created';
    $handler->display->display_options['fields']['created']['date_format'] = 'time ago';
    $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Template (field_template_ref) */
    $handler->display->display_options['arguments']['field_template_ref_target_id']['id'] = 'field_template_ref_target_id';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['table'] = 'field_data_field_template_ref';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['field'] = 'field_template_ref_target_id';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['default_action'] = 'not found';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['field_template_ref_target_id']['validate']['type'] = 'node';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['validate_options']['types'] = array(
      'site_template' => 'site_template',
    );
    $handler->display->display_options['path'] = 'node/%/sites';
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'Sites';
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;

    /* Display: Total sites */
    $handler = $view->new_display('page', 'Total sites', 'page_2');
    $handler->display->display_options['defaults']['group_by'] = FALSE;
    $handler->display->display_options['group_by'] = TRUE;
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'default';
    $handler->display->display_options['style_options']['default_row_class'] = FALSE;
    $handler->display->display_options['style_options']['row_class_special'] = FALSE;
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'fields';
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $handler->display->display_options['defaults']['fields'] = FALSE;
    /* Field: Content: Type */
    $handler->display->display_options['fields']['type_1']['id'] = 'type_1';
    $handler->display->display_options['fields']['type_1']['table'] = 'node';
    $handler->display->display_options['fields']['type_1']['field'] = 'type';
    $handler->display->display_options['fields']['type_1']['group_type'] = 'count';
    $handler->display->display_options['fields']['type_1']['label'] = '';
    $handler->display->display_options['fields']['type_1']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['type_1']['separator'] = '';
    $handler->display->display_options['defaults']['sorts'] = FALSE;
    $handler->display->display_options['defaults']['arguments'] = FALSE;
    /* Contextual filter: Content: Template (field_template_ref) */
    $handler->display->display_options['arguments']['field_template_ref_target_id']['id'] = 'field_template_ref_target_id';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['table'] = 'field_data_field_template_ref';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['field'] = 'field_template_ref_target_id';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['default_action'] = 'not found';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['default_argument_type'] = 'fixed';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary']['number_of_records'] = '0';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary']['format'] = 'default_summary';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['summary_options']['items_per_page'] = '25';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['specify_validation'] = TRUE;
    $handler->display->display_options['arguments']['field_template_ref_target_id']['validate']['type'] = 'node';
    $handler->display->display_options['arguments']['field_template_ref_target_id']['validate_options']['types'] = array(
      'site_template' => 'site_template',
    );
    $handler->display->display_options['path'] = 'node/%/sites';
    $handler->display->display_options['menu']['type'] = 'tab';
    $handler->display->display_options['menu']['title'] = 'Sites';
    $handler->display->display_options['menu']['weight'] = '0';
    $handler->display->display_options['menu']['context'] = 0;
    $handler->display->display_options['menu']['context_only_inline'] = 0;
    $views[$view->name] = $view;

    cache_clear_all('*', 'cache_menu', TRUE);
    return $views;
}
